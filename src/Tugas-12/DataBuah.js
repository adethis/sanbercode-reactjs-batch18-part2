import React, { Component } from 'react'
import '../Tugas-10/Table.css';

class DataBuah extends Component {
  constructor() {
    super()
    this.state = {
      dataHargaBuah: [
        { nama: "Semangka", harga: 10000, berat: 1000 },
        { nama: "Anggur", harga: 40000, berat: 500 },
        { nama: "Strawberry", harga: 30000, berat: 400 },
        { nama: "Jeruk", harga: 30000, berat: 1000 },
        { nama: "Mangga", harga: 30000, berat: 500 },
      ],
      nama: '',
      harga: 0,
      berat: 0,
      index: -1
    }
  }

  handleChange = evt => {
    this.setState({ [evt.target.name]: evt.target.value })
  }
  
  handleEdit = evt => {
    let id = evt.target.id
    this.setState({
      index: id,
      nama: this.state.dataHargaBuah[id].nama,
      harga: this.state.dataHargaBuah[id].harga,
      berat: this.state.dataHargaBuah[id].berat
    })
  }

  handleDelete = evt => {
    let id = evt.target.id
    this.state.dataHargaBuah.splice(id, 1)
    this.setState({ dataHargaBuah: this.state.dataHargaBuah })
  }

  handleSubmit = evt => {
    evt.preventDefault()
    let index = this.state.index
    let nama = this.state.nama
    let harga = this.state.harga
    let berat = this.state.berat
    let input = { nama, harga, berat }
    let dataHargaBuah = this.state.dataHargaBuah
    if (index === -1) {
      this.setState({
        dataHargaBuah: [...dataHargaBuah, input],
        nama: '', harga: 0, berat: 0
      })
    } else {
      dataHargaBuah[index] = input
      this.setState({
        dataHargaBuah,
        nama: '', harga: 0, berat: 0, index: -1
      })
    }
  }

  render() {
    return (
      <div className="sectionForm" style={{ margin: '30px auto' }}>
        <form onSubmit={this.handleSubmit} style={{ display: 'block', margin: '30px auto' }}>
          <label>
            Nama: 
            <input 
              type='text'
              name='nama'
              value={this.state.nama}
              onChange={this.handleChange} 
              required />
          </label>
          |
          <label>
            Harga:
            <input 
              type='number'
              name='harga' 
              value={this.state.harga}
              onChange={this.handleChange} 
              required />
          </label>
          |
          <label>
            Berat:
            <input 
              type='number'
              name='berat' 
              value={this.state.berat}
              onChange={this.handleChange} 
              required />
          </label>
          <p style={{ textAlign: 'right' }}>
            <input type='submit'/>
          </p>
        </form>
        <h2>Table Harga Buah</h2>
        <table border="0" cellSpacing="2" cellPadding="10" className="sectionTable" width="100%">
          <thead className="bg-grey">
            <tr>
              <th>Nama</th>
              <th>Harga</th>
              <th>Berat</th>
              <th colSpan="2">Aksi</th>
            </tr>
          </thead>
          <tbody className="bg-orange">
            {
              this.state.dataHargaBuah.map((item, index) => {
                return <tr key={ index }>
                  <td>{ item.nama }</td>
                  <td>{ item.harga }</td>
                  <td>{ item.berat / 1000 } kg</td>
                  <td><button id={index} onClick={this.handleEdit}>Edit</button></td>
                  <td><button id={index} onClick={this.handleDelete}>Delete</button></td>
                </tr>
              })
            }
          </tbody>
        </table>
      </div>
    )
  }
}

export default DataBuah