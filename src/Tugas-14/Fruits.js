import React from 'react'
import { FruitsProvider } from './FruitsContext'
import FruitsList from './FruitsList'
import FruitsForm from './FruitsForm'
import './Fruits.css'

const Fruits = () => {
  return (
    <FruitsProvider>
      <div className="parentElement">
        <h1>Data Buah</h1>
        <FruitsForm />
        <FruitsList />
      </div>
    </FruitsProvider>
  );
}

export default Fruits