import React, { useContext } from 'react'
import { FruitsContext } from './FruitsContext'
import axios from 'axios'

const FruitsForm = () => {
  const [
    dataBuah, 
    setDataBuah, 
    fetchData,
    inputBuah,
    setInputBuah,
    input,
    handleChange
  ] = useContext(FruitsContext)
  
  const handleSubmit = evt => {
    evt.preventDefault()
    let { name, price, weight } = inputBuah
    if (inputBuah.id === null) {
      axios.post('http://backendexample.sanbercloud.com/api/fruits', { name, price, weight })
           .then(res => {
             setDataBuah([...dataBuah, res.data])
             setInputBuah(input)
             fetchData()
           })
    } else {
      axios.put(`http://backendexample.sanbercloud.com/api/fruits/${inputBuah.id}`, { name, price, weight })
           .then(res => {
             let data = dataBuah.map(item => {
               if (item.id === inputBuah.id) {
                 item.name = inputBuah.name
                 item.price = inputBuah.price
                 item.weight = inputBuah.weight
               }
               return item
             })
             setDataBuah(data)
             setInputBuah(input)
             fetchData()
           })
    }
  }

  return (
    <div className='childElement'>
      <form onSubmit={handleSubmit}>
        <div>
          <label>Name :</label>
          <input 
            type='text'
            name='name'
            value={inputBuah.name}
            onChange={handleChange}
            required />
        </div>
        <div>
          <label>Price :</label>
          <input 
            type='text'
            name='price'
            value={inputBuah.price}
            onChange={handleChange}
            required />
        </div>
        <div>
          <label>Weight :</label>
          <input 
            type='number'
            name='weight'
            value={inputBuah.weight}
            onChange={handleChange}
            required />
        </div>
        <div><label>&nbsp;</label><input type='submit' value='Submit' /></div>
      </form>
    </div>
  )
}

export default FruitsForm