import React, { useContext } from 'react'
import { FruitsContext } from './FruitsContext'
import axios from 'axios'

const FruitsList = () => {
  const [
    dataBuah, 
    setDataBuah, 
    fetchData,
    inputBuah,
    setInputBuah
  ] = useContext(FruitsContext)
  
  const handleEdit = evt => {
    let id = parseInt(evt.target.value)
    let fruits = dataBuah.find(item => item.id === id)
    let {name, price, weight } = fruits
    setInputBuah({
      ...inputBuah, id: id, name, price, weight
    })
  }

  const handleDelete = evt => {
    let id = evt.target.value
    axios.delete(`http://backendexample.sanbercloud.com/api/fruits/${id}`)
         .then(res => {
           let data = dataBuah.filter(item => item.id !== id)
           setDataBuah(data)
           fetchData()
         })
  }

  return (
    <div className='childElement'>
      <table border='1' cellPadding='10' width='100%'>
        <thead>
          <tr>
            <th>Name</th>
            <th>Price</th>
            <th>Weight</th>
            <th colSpan='2'>Actions</th>
          </tr>
        </thead>
        <tbody>
          {
            dataBuah !== null &&
            dataBuah.map((value, index) => {
              return (
                <tr key={index}>
                  <td>{value.name}</td>
                  <td>{value.price}</td>
                  <td>{value.weight / 1000}kg</td>
                  <td>
                    <button value={value.id} onClick={handleEdit}>Edit</button>
                  </td>
                  <td>
                    <button value={value.id} onClick={handleDelete}>Delete</button>
                  </td>
                </tr>
              )
            })
          }
        </tbody>
      </table>
    </div>
  )
}

export default FruitsList