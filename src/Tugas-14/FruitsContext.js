import React, { useState, useEffect, createContext } from 'react'
import axios from 'axios'

export const FruitsContext = createContext()

export const FruitsProvider = props => {
  const input = { name: "", price: "", weight: 0, id: null }
  const [dataBuah, setDataBuah] = useState(null)
  const [inputBuah, setInputBuah] = useState(input)

  const fetchData = () => {
    return (
      axios.get('http://backendexample.sanbercloud.com/api/fruits')
           .then(res => {
             setDataBuah(res.data)
           })
    )
  }

  useEffect(() => {
    fetchData()
  }, [])

  const handleChange = evt => {
    let name = evt.target.name
    let value = evt.target.value
    setInputBuah({...inputBuah, [name]: value})
  }

  return (
    <FruitsContext.Provider 
      value={[
        dataBuah, 
        setDataBuah, 
        fetchData,
        inputBuah,
        setInputBuah,
        input,
        handleChange ]}>
      {props.children}
    </FruitsContext.Provider>
  )
}