import React, { Component } from "react";
import "./Table.css";

class Table extends Component {
  render() {
    const dataBuah = [
      {nama: "Semangka", harga: 10000, berat: 1000},
      {nama: "Anggur", harga: 40000, berat: 500},
      {nama: "Strawberry", harga: 30000, berat: 400},
      {nama: "Jeruk", harga: 30000, berat: 1000},
      {nama: "Mangga", harga: 30000, berat: 500}
    ]
    return (
      <div className="sectionForm list">
        <h2>Table Harga Buah</h2>
        <table
          border="1"
          cellSpacing="2"
          cellPadding="8"
          className="sectionTable"
          width="100%">
          <thead className="bg-grey">
            <tr>
              <th>Nama</th>
              <th>Harga</th>
              <th>Berat</th>
            </tr>
          </thead>
          <tbody className="bg-orange">
            {dataBuah.map((item, key) => {
              return (
                <tr key={key}>
                  <td>{item.nama}</td>
                  <td>{item.harga}</td>
                  <td>{item.berat / 1000} kg</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}

export default Table;
