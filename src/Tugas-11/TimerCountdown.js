import React, { Component } from 'react'
import Timer from './Timer'
import Countdown from './Countdown'
import './TimerCountdown.css'

class TimerCountdown extends Component {
  constructor(props) {
    super(props)
    this.state = {
      date: new Date(),
      timer: 100,
      status: true
    }
  }

  handleStatus = e => {
    setTimeout(() => {
      this.setState({status: e})
    }, 800)
  }
  
  render() {
    return (
      <>
        {
          this.state.status
          ?
          <div className='sectionCounter'>
            <div className='parentElement'>
              <Timer
                date={this.state.date} 
                className='childElement' />
              <Countdown 
                start={this.state.timer} 
                handleStatus={this.handleStatus}
                className='childElement' />
            </div>
          </div>
          : null
        }
      </>
    )
  }
}

export default TimerCountdown