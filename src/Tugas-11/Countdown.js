import React, { Component } from 'react'

class Countdown extends Component {
  constructor(props) {
    super(props)
    this.state = {
      time: this.props.start !== undefined ? this.props.start : 100
    }
  }

  componentDidMount() {
    this.timerID = setInterval(() => this.tick(), 1000)
  }

  componentDidUpdate(p, s) {
    if (s.time <= 1) {
      this.props.handleStatus(false)
    }
  }

  componentWillUnmount() {
    clearInterval(this.timerID)
  }
  
  tick() {
    this.setState({
      time: this.state.time - 1
    })
  }

  render() {
    return (
      <div className={this.props.className}>Hitung mundur: {this.state.time}</div>
    )
  }
}

export default Countdown