import React, { useContext } from 'react'
import { ThemeContext } from '../Context/ThemeContext'

const ToggleMode = () => {
  const [dataTheme, setDataTheme] = useContext(ThemeContext)

  const handleToggle = evt => {
    const checked = evt.target.checked
    setDataTheme(checked)
    setGlobalCss('html', 'dark', 'light')
  }

  const setGlobalCss = (el, dark, light) => {
    let elm = document.getElementsByTagName(el);
    elm[0].setAttribute(
      "data-theme",
      elm[0].getAttribute("data-theme") === dark ? light : dark
    )
  }

  return (
    <>
      <div className="tools">
        <div>Dark Mode</div>
        <div className="theme-toggle">
          <input onChange={handleToggle} checked={dataTheme} type="checkbox" className="switch" />
          <span></span>
        </div>
      </div>
    </>
  );
}

export default ToggleMode