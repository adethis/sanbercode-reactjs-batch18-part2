import React, { useContext } from 'react'
import { Link } from 'react-router-dom'
import { ThemeContext } from '../Context/ThemeContext'
import ToggleMode from './ToggleMode'

const Navbar = () => {
  const [ dataTheme ] = useContext(ThemeContext)

  return (
    <>
      <header className='navbar'>
        <nav data-theme={`${dataTheme ? 'dark' : 'light'}`}>
          <div className='logo'>Pertugasan</div>
          <ul>
            <li><Link to='/'>Home</Link></li>
            <li><Link to='/tugas9'>Tugas9</Link></li>
            <li><Link to='/tugas10'>Tugas10</Link></li>
            <li><Link to='/tugas11'>Tugas11</Link></li>
            <li><Link to='/tugas12'>Tugas12</Link></li>
            <li><Link to='/tugas13'>Tugas13</Link></li>
            <li><Link to='/tugas14'>Tugas14</Link></li>
          </ul>
          <ToggleMode />
        </nav>
      </header>
    </>
  )
}

export default Navbar