import React, { useState, createContext } from 'react'

export const ThemeContext = createContext()

export const ThemeProvider = props => {
  const [dataTheme, setDataTheme] = useState(false)

  return (
    <ThemeContext.Provider value={[dataTheme, setDataTheme]}>
      {props.children}
    </ThemeContext.Provider>
  )
}