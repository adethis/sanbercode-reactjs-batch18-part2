import React from 'react'
import { Switch, Route } from 'react-router'
import Home from '../Page/Home'
import Form from '../../Tugas-9/Form'
import Table from '../../Tugas-10/Table'
import TimerCountdown from '../../Tugas-11/TimerCountdown'
import DataBuah from '../../Tugas-12/DataBuah'
import Tugas13 from '../../Tugas-13/Tugas13'
import Fruits from '../../Tugas-14/Fruits'

const Routes = () => {
  return (
    <>
      <section className="content">
        <Switch>
          <Route exact path="/">
            <Home />
          </Route>
          <Route path="/tugas9">
            <Form />
          </Route>
          <Route path="/tugas10">
            <Table />
          </Route>
          <Route path="/tugas11">
            <TimerCountdown />
          </Route>
          <Route path="/tugas12">
            <DataBuah />
          </Route>
          <Route path="/tugas13">
            <Tugas13 />
          </Route>
          <Route path="/tugas14">
            <Fruits />
          </Route>
        </Switch>
      </section>
    </>
  );
}

export default Routes