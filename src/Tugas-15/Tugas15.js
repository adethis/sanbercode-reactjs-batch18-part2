import React from 'react'
import { BrowserRouter as Router } from 'react-router-dom'
import Navbar from './Component/Navbar'
import Routes from './Routes/Routes'
import { ThemeProvider } from './Context/ThemeContext'
import './Style.css'

const Tugas15 = () => {
  return (
    <div>
      <Router>
        <ThemeProvider>
        <Navbar />
        <Routes />
        </ThemeProvider>
      </Router>
    </div>
  )
}

export default Tugas15