import React, { useContext } from 'react'
import ToggleMode from '../Component/ToggleMode'
import { ThemeContext } from '../Context/ThemeContext'

const Home = () => {
  const [dataTheme] = useContext(ThemeContext);

  return (
    <div className='childComponent' data-theme={`${dataTheme ? 'dark' : 'light'}`}>
      <h1>Home <small>(Tugas 15)</small></h1>
      <ToggleMode />
    </div>
  )
}

export default Home