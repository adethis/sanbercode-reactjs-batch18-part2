import React, { useState, useEffect } from 'react'
import axios from 'axios'
import './Tugas13.css'

const Tugas13 = () => {
  const input = { id: null, name: '', price: '', weight: 0 }
  const [dataBuah, setDataBuah] = useState(null)
  const [inputBuah, setInputBuah] = useState(input)

  function fetchData() {
    return (
      axios.get('http://backendexample.sanbercloud.com/api/fruits')
           .then(res => {
             setDataBuah(res.data)
           })
    )
  }

  useEffect(() => {
    fetchData()
  }, [])

  const handleChange = evt => {
    let name = evt.target.name
    let value = evt.target.value
    setInputBuah({...inputBuah, [name]: value})
  }

  const handleEdit = evt => {
    let id = parseInt(evt.target.value)
    let fruits = dataBuah.find(item => item.id === id)
    setInputBuah({id: id, name: fruits.name, price: fruits.price, weight: fruits.weight})
  }
  
  const handleDelete = evt => {
    let id = evt.target.value
    axios.delete(`http://backendexample.sanbercloud.com/api/fruits/${id}`)
         .then(res => {
           let data = dataBuah.filter(item => item.id !== id)
           setDataBuah(data)
           fetchData()
         })
  }

  const handleSubmit = evt => {
    evt.preventDefault()
    if (inputBuah.id === null) {
      axios.post('http://backendexample.sanbercloud.com/api/fruits', { name: inputBuah.name, price: inputBuah.price, weight: inputBuah.weight })
           .then(res => {
             setDataBuah([...dataBuah, res.data])
             setInputBuah(input)
             fetchData()
           })
    } else {
      axios.put(`http://backendexample.sanbercloud.com/api/fruits/${inputBuah.id}`, { name: inputBuah.name, price: inputBuah.price, weight: inputBuah.weight })
           .then(res => {
             let data = dataBuah.map(item => {
               if (item.id === inputBuah.id) {
                 item.name = inputBuah.name
                 item.price = inputBuah.price
                 item.weight = inputBuah.weight
               }
               return item
             })
             setDataBuah(data)
             setInputBuah(input)
             fetchData()
           })
    }
  }

  return (
    <div className='parentElement'>
      <h1>Data Buah</h1>
      <div className='childElement'>
        <form onSubmit={handleSubmit}>
          <div>
            <label>Name :</label>
            <input 
              type='text'
              name='name'
              value={inputBuah.name}
              onChange={handleChange}
              required />
          </div>
          <div>
            <label>Price :</label>
            <input 
              type='text'
              name='price'
              value={inputBuah.price}
              onChange={handleChange}
              required />
          </div>
          <div>
            <label>Weight :</label>
            <input 
              type='number'
              name='weight'
              value={inputBuah.weight}
              onChange={handleChange}
              required />
          </div>
          <div><label>&nbsp;</label><input type='submit' value='Submit' /></div>
        </form>
      </div>
      <div className='childElement'>
        <table border='1' cellPadding='10' width='100%'>
          <thead>
            <tr>
              <th>Name</th>
              <th>Price</th>
              <th>Weight</th>
              <th colSpan='2'>Actions</th>
            </tr>
          </thead>
          <tbody>
            {
              dataBuah !== null &&
              dataBuah.map((value, index) => {
                return (
                  <tr key={index}>
                    <td>{value.name}</td>
                    <td>{value.price}</td>
                    <td>{value.weight / 1000}kg</td>
                    <td>
                      <button value={value.id} onClick={handleEdit}>Edit</button>
                    </td>
                    <td>
                      <button value={value.id} onClick={handleDelete}>Delete</button>
                    </td>
                  </tr>
                )
              })
            }
          </tbody>
        </table>
      </div>
    </div>
  )

}

export default Tugas13