import React, { Component } from "react";
import "./Form.css";

class Form extends Component {
  render() {
    const dataBuah = [
      {nama: "Semangka", harga: 10000, berat: 1000},
      {nama: "Anggur", harga: 40000, berat: 500},
      {nama: "Strawberry", harga: 30000, berat: 400},
      {nama: "Jeruk", harga: 30000, berat: 1000},
      {nama: "Mangga", harga: 30000, berat: 500}
    ]
    return (
      <div className="sectionForm">
        <h2>Form Pembelian Buah</h2>
        <table 
          border="0" 
          cellPadding="1" 
          width="100%" 
          className="sectionTable">
          <tbody>
            <tr>
              <td>Nama Pelanggan</td>
              <td>
                <input type="text" className="inputStyle" />
              </td>
            </tr>
            <tr>
              <td valign="bottom" style={{ paddingBottom: "18px" }}>
                Daftar item
              </td>
              <td>
                {dataBuah.map((item, key) => {
                  return (
                    <p key={key}>
                      <label>
                        <input type="checkbox" /> {item.nama}
                      </label>
                    </p>
                  );
                })}
              </td>
            </tr>
            <tr>
              <td colSpan="2">
                <button className="buttonStyle">
                  Kirim
                </button>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
}

export default Form;
